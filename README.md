# manual-npm-release

This is a POC repository to demonstrate manual releases via GitLab CI.

The idea is the following: Semantic release might be too much for smaller projects, and you want to release.
Unfortunately certain aspects of the release process are error prone (need to remember to push to the registry, creating tags and maybe even creating GitLab releases)

Fortunately semantic-release allows for plugins and one of them is [`@semantic-release/exec`](https://github.com/semantic-release/exec/blob/master/README.md) which allows for arbitrary execution of scripts.

The idea now is really simple:
1. Have one script which checks an environment variable (CI_RELEASE_VERSION) and if it is set to either major, minor or patch, release the appropriate version
2. In order to enhance the notes of the release to describe the actual changes, one can use (CI_RELEASE_DESCRIPTION). Otherwise the description will be something like "Bumping version from v1.0.0 to v1.1.0"

A maintainer of the project could now go and trigger a manual pipeline via: https://gitlab.com/gitlab-org/frontend/playground/manual-npm-release/-/pipelines/new and set the variable.

Semantic release has the following config:

```yaml
"plugins": [
    [
    "@semantic-release/exec",
    {
        "analyzeCommitsCmd": "./build_scripts/get_version_from_env.sh ${lastRelease.gitTag}",
        "generateNotesCmd": "./build_scripts/get_notes_from_env.sh ${lastRelease.gitTag} ${nextRelease.version}"
    }
    ],
    [
    "@semantic-release/npm",
    {
        "npmPublish": false,
        "tarballDir": "dist"
    }
    ],
    "@semantic-release/git",
    [
    "@semantic-release/gitlab",
    {
        "assets": [
        {
            "path": "dist/*.tgz",
            "label": "CSS distribution"
        }
        ]
    }
    ]
]
```

This will lead to the following execution:

1. `verifyConditions`: here the plugins check if all variables are set (most notably `GITLAB_TOKEN` needs to be set for the git and gitlab plugin
2. `analyzeCommits`: Semantic release would usually analyze the commits (e.g. with `@semantic-release/changelog`), but here our bash script just checks whether `CI_RELEASE_VERSION` is set and adjusts the version accordingly.
3. `generateNotes`: Notes are taken from `CI_RELEASE_DESCRIPTION`
4. `prepare`: Prepare git tag and commit the new version with the notes. Bumping version via `npm` and creating a release tarball.
5. `publish`: Publish new version via a git push and a gitlab release via the API, uploading the tarball.

It is useful to use project-level access token or a token from a bot. This token (`GITLAB_TOKEN`) should be set via the CI/CD settings, definitely masked and maybe "protected".
After the project-level access token is used for the first time, the push rights to the default branch could be limited to that user.