#!/bin/sh

# This also could be bash, but the semantic-release image doesn't have bash installed :(

CI_RELEASE_DESCRIPTION=${CI_RELEASE_DESCRIPTION:-""}

>&2 echo 'Checking whether we CI_RELEASE_DESCRIPTION is set'

## We could also check whether CI_RELEASE_DESCRIPTION IS SET

if [ -n "$CI_RELEASE_DESCRIPTION" ]; then
echo "$CI_RELEASE_DESCRIPTION"
else
echo "Bumping version from $1 to $2"
fi

exit 0
