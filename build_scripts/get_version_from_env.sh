#!/bin/sh

# This also could be bash, but the semantic-release image doesn't have bash installed :(

CI_RELEASE_VERSION=${CI_RELEASE_VERSION:-"null"}

>&2 echo 'Checking whether we release from the ENV (CI_RELEASE_VERSION is set) variables:'

## We could also check whether CI_RELEASE_DESCRIPTION IS SET

case "$CI_RELEASE_VERSION" in
    major|minor|patch )
        >&2 echo "CI_RELEASE_VERSION correctly set, will release $CI_RELEASE_VERSION version"
        echo "$CI_RELEASE_VERSION"
    ;;
    *) >&2 echo "CI_RELEASE_VERSION ($CI_RELEASE_VERSION) not set to either major, minor or patch => No release" ;;
esac

exit 0
